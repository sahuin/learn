def initial (name, toUpper = True):
    if(toUpper):
        return(name[0].upper())
    else:
        return(name[0])


myName = input('Enter your first name: ')
print(initial(myName))
print(initial(toUpper = False, name=myName))
